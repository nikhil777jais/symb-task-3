from django.contrib import admin
from django.urls import path
from . import views
from rest_framework_simplejwt import views as jwt_views
urlpatterns = [
    path('ts/', views.TeacherSignUpAPI.as_view(), name='ts' ),
    path('ss/', views.StudentSignUpAPI.as_view(), name='ss' ),
    path('t/', views.TecherAPI.as_view(), name='t' ),
    path('t/<int:id>/', views.TecherAPI.as_view(), name='tid' ),
    path('s/', views.StudentAPI.as_view(), name='s' ),
    path('erp/', views.CollegeERPAPI.as_view(), name='erp' ),
    path('erp/<int:id>/', views.CollegeERPAPI.as_view(), name='erpid' ),
    path('gettoken/', jwt_views.TokenObtainPairView.as_view(), name='gettoken'),
    path('refresh/', jwt_views.TokenRefreshView.as_view(), name='refresh')
]
