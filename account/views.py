from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import TeacherSignUpSerializer, TeacherSerializer, StudentSerializer, StudentSignUpSerializer
from django.shortcuts import get_object_or_404
from .models import Teacher, Student
from django.contrib.auth import get_user_model
from account import models
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import DjangoModelPermissions
from rest_framework_simplejwt.tokens import RefreshToken
User = get_user_model()
# Create your views here.

class TeacherSignUpAPI(APIView):
  def post(self, request, format = None):
    request.data['profile'] = 'teacher'
    serializer = TeacherSignUpSerializer(data=request.data) #request.data conatain the data that come from post request
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email']) #get newly generated user
      refresh = RefreshToken.for_user(user) #generate a token for user just while signup
      return Response({'payload': serializer.data ,'refresh': str(refresh), 'access': str(refresh.access_token),'msg':'Data Saved!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)    

class StudentSignUpAPI(APIView):
  def post(self, request, format = None):
    request.data['profile'] = 'student'
    serializer = StudentSignUpSerializer(data=request.data) #request.data conatain the data that come from post request
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email']) #get newly generated user
      refresh = RefreshToken.for_user(user) #generate a token for user just while signup
      return Response({'payload': serializer.data,'refresh': str(refresh), 'access': str(refresh.access_token),'msg':'Data Saved!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)    

class StudentAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()
  def get(self, request, format=None):
    user = User.objects.get(email = request.data['email']) #the user who is currently loggedIn
    if user.profile == 'student':
      student_obj = Student.objects.get(user=user.id) #getting Student data realted to that user
      serializer = StudentSerializer(student_obj)
      return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
    return Response({'msg':'only Student can access!!'}, status = status.HTTP_400_BAD_REQUEST)    

class TecherAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()

  def get(self, request, id=None, format=None):
    if request.user.profile != 'teacher':
      return Response({'msg':'unauthorized access!'}, status = status.HTTP_401_UNAUTHORIZED)
    teacher_obj = Teacher.objects.get(user=request.user.id)
    serializer_t = TeacherSerializer(teacher_obj)
    if not id == None:
      user_obj = Student.objects.get(pk=id)
      serializer = StudentSerializer(user_obj)
      return Response({'teacher_info':serializer_t.data,'payload': serializer.data, 'msg':'Data Retrived!!'},status = status.HTTP_200_OK)
    user_obj = Student.objects.all()
    serializer = StudentSerializer(user_obj, many= True)
    return Response({'teacher_info':serializer_t.data,'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)

  def put(self, request, id = None, format = None):
    if request.user.profile != 'teacher':
      return Response({'msg':'unauthorized access!'}, status = status.HTTP_401_UNAUTHORIZED)
    student_obj = Student.objects.get(pk=id)
    serializer = StudentSerializer(student_obj, data = request.data) #request.data get data from user instance
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data, 'msg':'Data Upadted Completely!!'}, status = status.HTTP_200_OK)
    return Response(serializer.errors, status = status.HTTP_401_UNAUTHORIZED)

  def patch(self, request, id = None, format = None):
    if request.user.profile != 'teacher':
      return Response({'msg':'unauthorized access!'}, status = status.HTTP_401_UNAUTHORIZED)
    user_obj = Student.objects.get(pk=id)
    serializer = StudentSerializer(user_obj, data = request.data, partial = True) #request.data get data from user ins
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data, 'msg':'Data Upadted Patially!!'}, status = status.HTTP_200_OK)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  
    

class CollegeERPAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()
  def get(self, request, id=None, format=None):
    if request.user.profile != 'Admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    if not id == None:
      user_obj = User.objects.get(pk=id)
      if user_obj.profile == 'teacher':
        serializer = TeacherSignUpSerializer(user_obj)
      elif user_obj.profile == 'student':
        serializer = StudentSignUpSerializer(user_obj)
      return Response({'payload':serializer.data,'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
    user_obj_t = User.objects.filter(profile='teacher')
    user_obj_s = User.objects.filter(profile='student')
    serializer_t =TeacherSignUpSerializer(user_obj_t, many= True)
    serializer_s =StudentSignUpSerializer(user_obj_s, many= True)
    return Response({'payload':{'teacher':serializer_t.data, 'student':serializer_s.data},'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
    
  def post(self, request, format = None):
    if request.user.profile != 'Admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    if request.data['profile'] == 'teacher':
      serializer = TeacherSignUpSerializer(data=request.data) 
    elif request.data['profile'] == 'student':
      serializer = StudentSignUpSerializer(data=request.data)
    else:  
      return Response({'msg':'profile is mandatory'}, status = status.HTTP_400_BAD_REQUEST) 
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email']) #get newly generated user
      refresh = RefreshToken.for_user(user) #generate a token for user just while signup
      return Response({'payload': serializer.data ,'refresh': str(refresh), 'access': str(refresh.access_token),'msg':'Data Saved!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) 
  
  def put(self, request, id, format = None):
    if request.user.profile != 'Admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    user_obj = User.objects.get(pk=id)
    if user_obj.profile == 'teacher':
      serializer = TeacherSignUpSerializer(user_obj, data = request.data)
    elif user_obj.profile == 'student':
      serializer = StudentSignUpSerializer(user_obj, data = request.data)
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data ,'msg':'Data updated Completely!!'}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) 

  def patch(self, request, id, format = None):
    if request.user.profile != 'Admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    user_obj = User.objects.get(pk=id)
    if user_obj.profile == 'teacher':
      serializer = TeacherSignUpSerializer(user_obj, data = request.data, partial = True)
    elif user_obj.profile == 'student':
      serializer = StudentSignUpSerializer(user_obj, data = request.data, partial = True)
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data ,'msg':'Data updated partially!!'}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) 
  
  def delete(self, request, id = None, format = None):
    if request.user.profile != 'Admin':
      return Response({'msg':'unauthorized access!!'}, status = status.HTTP_401_UNAUTHORIZED)
    user = User.objects.get(pk=id)
    user.delete() 
    return Response({'msg':'Data Deleted Completely!!'}, status = status.HTTP_200_OK)  